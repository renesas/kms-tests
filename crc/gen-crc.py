#!/usr/bin/python3
# SPDX-License-Identifier: GPL-2.0-or-later
# SPDX-FileCopyrightText: 2020 Laurent Pinchart <laurent.pinchart@ideasonboard.com>

import crcmod
import sys

# The initial value is ignored, it must instead be passed to the generated C
# function.
crc = crcmod.Crc(0x104c11db7, 0xffffffff, True, 0xffffffff)

f = open(sys.argv[1], 'w')
crc.generateCode('calculate_crc', f, 'uint8_t', 'uint32_t')
f.close()

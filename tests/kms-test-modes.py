#!/usr/bin/python3
# SPDX-License-Identifier: GPL-2.0-or-later
# SPDX-FileCopyrightText: 2017-2019 Renesas Electronics Corporation

import kmstest
import pykms

class ModesTest(kmstest.KMSTest):
    """Test all available modes on all available connectors."""

    def handle_page_flip(self, frame, time):
        self.logger.log('Page flip complete')

    def test_mode(self, connector, crtc, mode):
        self.logger.log(f'Testing connector {connector.fullname} '
                        f'on CRTC {crtc.id} with mode {mode.name}')

        # Create a frame buffer
        fb = pykms.DumbFramebuffer(self.card, mode.hdisplay, mode.vdisplay, 'XR24')
        pykms.draw_test_pattern(fb)

        # Perform the mode set
        ret = self.atomic_crtc_mode_set(crtc, connector, mode, fb)
        if ret < 0:
            raise RuntimeError(f'atomic mode set failed with {ret}')

        self.logger.log('Atomic mode set complete')
        self.run(4)
        self.atomic_crtc_disable(crtc)

        if self.flips == 0:
            raise RuntimeError('Page flip not registered')

    def main(self):
        for connector in self.output_connectors():
            self.start(f'modes on connector {connector.fullname}')

            # Skip disconnected connectors
            if not connector.connected():
                self.skip('unconnected connector')
                continue

            # Find a CRTC suitable for the connector
            crtc = connector.get_current_crtc()
            if not crtc:
                crtcs = connector.get_possible_crtcs()
                if len(crtcs) == 0:
                    pass

                crtc = crtcs[0]

            # Test all available modes
            modes = connector.get_modes()
            if len(modes) == 0:
                self.skip('no mode available')
                continue

            for i in range(len(modes)):
                try:
                    self.progress(i+1, len(modes))
                    self.test_mode(connector, crtc, modes[i])
                except RuntimeError as e:
                    self.fail(e.args[0])
                    break
            else:
                self.success()


if __name__ == '__main__':
    ModesTest().execute()

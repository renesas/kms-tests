#!/usr/bin/python3
# SPDX-License-Identifier: GPL-2.0-or-later
# SPDX-FileCopyrightText: 2022 Renesas Electronics Corporation

import kmstest
import pykms

class PlaneBlendModeTest(kmstest.KMSTest):
    """Test composition in different blend modes."""

    def handle_page_flip(self, frame, time):
        self.logger.log('Page flip complete')

    def find_pipeline(self):
        # Find a CRTC that has multiple planes with a connected connector
        for connector in self.output_connectors():
            # Skip disconnected connectors
            if not connector.connected():
                continue

            # Add the connector to the map
            for crtc in connector.get_possible_crtcs():
                planes = []
                for plane in self.card.planes:
                    if plane.supports_crtc(crtc) and plane != crtc.primary_plane:
                        planes.append(plane)

                if len(planes):
                    return crtc, connector, planes

        return None, None, None

    def main(self):
        self.start('composition with blend modes')

        crtc, connector, planes = self.find_pipeline()
        if crtc is None:
            self.skip('no suitable pipeline')
            return

        # Get the default mode
        try:
            mode = connector.get_default_mode()
        except KeyError:
            self.skip('no mode available')
            return

        self.logger.log(f'Testing connector {connector.fullname}, CRTC {crtc.id}, '
                        f'mode {mode.name} with {len(planes)} planes '
                        f'(P: {crtc.primary_plane.id}, O: {[plane.id for plane in planes]})')

        # Create a frame buffer for the primary plane
        fb_primary = pykms.DumbFramebuffer(self.card, mode.hdisplay, mode.vdisplay, 'XR24')
        pykms.draw_test_pattern(fb_primary)

        # Create a frame buffer for the overlay planes (half of the screen size)
        fb_overlay = pykms.DumbFramebuffer(self.card, mode.hdisplay // 2, mode.vdisplay, 'AR24')
        self.logger.log(f'Create fb: {fb_overlay} ({fb_overlay.width}x{fb_overlay.height})')
        width = mode.hdisplay // 4
        height = mode.vdisplay // 5
        for n in range(0, 5):
            v = 255 - 63 * n
            pykms.draw_rect(fb_overlay, 0, height * n, width, height,
                            pykms.RGB(v, 255, 255, 255))
            pykms.draw_rect(fb_overlay, width, height * n, width, height,
                            pykms.RGB(v, v, v, v))

        # Set the mode with a primary plane
        ret = self.atomic_crtc_mode_set(crtc, connector, mode, fb_primary)
        if ret < 0:
            self.fail(f'atomic mode set failed with {ret}')
            return

        self.run(3)

        # Add the overlay plane, testing all blend modes sequentially
        source = kmstest.Rect(0, 0, fb_overlay.width, fb_overlay.height)
        destination = kmstest.Rect(fb_overlay.width, 0, fb_overlay.width, fb_overlay.height)
        alpha = '50%'
        for blendmode in ('Pre-multiplied', 'Coverage', 'None'):
            pykms.draw_text(fb_overlay, 10, 10, f'alpha={alpha}, mode={blendmode}          ',
                            pykms.white)
            ret = self.atomic_plane_set(planes[1], crtc, source, destination,
                                        fb_overlay, alpha=alpha, blendmode=blendmode)
            if ret < 0:
                self.fail(f'atomic plane set failed with {ret}')
                break

            self.logger.log(f'Adding plane {planes[1].id} with blendmode {blendmode}')
            self.run(1)

            if self.flips == 0:
                self.fail('No page flip registered')
                break

            self.run(3)

        else:
            self.success()

        self.atomic_crtc_disable(crtc)


if __name__ == '__main__':
    PlaneBlendModeTest().execute()

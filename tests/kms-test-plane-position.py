#!/usr/bin/python3
# SPDX-License-Identifier: GPL-2.0-or-later
# SPDX-FileCopyrightText: 2017-2019 Renesas Electronics Corporation

import kmstest
import pykms
import time

class PlanePositionTest(kmstest.KMSTest):
    """Test boundaries of plane positioning."""

    def main(self):
        self.start('plane positioning boundaries')

        # Find a CRTC with a connected connector and at least two planes
        for connector in self.output_connectors():
            if not connector.connected():
                self.logger.log(f'unconnected connector {connector.fullname}')
                continue

            try:
                mode = connector.get_default_mode()
            except ValueError:
                continue

            crtcs = connector.get_possible_crtcs()
            for crtc in crtcs:
                planes = []
                for plane in self.card.planes:
                    if plane.supports_crtc(crtc):
                        planes.append(plane)

                if len(planes) > 1:
                    break
            else:
                crtc = None

            if crtc:
                break

        else:
            self.skip('no CRTC available with connector and at least two planes')
            return

        self.logger.log(f'Testing connector {connector.fullname}, CRTC {crtc.id}, '
                        f'mode {mode.name} with {len(planes)} planes')

        # Create a frame buffer
        fb = pykms.DumbFramebuffer(self.card, mode.hdisplay, mode.vdisplay, 'XR24')
        pykms.draw_test_pattern(fb)

        # Set the mode with no plane, wait 5s for the monitor to wake up
        ret = self.atomic_crtc_mode_set(crtc, connector, mode, sync=True)
        if ret < 0:
            self.fail(f'atomic mode set failed with {ret}')
            return

        self.logger.log('Initial atomic mode set completed')
        time.sleep(5)

        # Add the first plane to cover half of the CRTC
        source = kmstest.Rect(0, 0, fb.width // 2, fb.height)
        destination = kmstest.Rect(0, 0, fb.width // 2, fb.height)
        ret = self.atomic_plane_set(planes[0], crtc, source, destination, fb, sync=True)
        if ret < 0:
            self.fail(f'atomic plane set for first plane failed with {ret}')
            return

        self.logger.log('Root plane enabled')
        time.sleep(3)

        # Add the second plane and move it around to cross all CRTC boundaries
        offsets = ((50, 50), (150, 50), (50, 150), (-50, 50), (50, -50))
        for offset in offsets:
            width = fb.width - 100
            height = fb.height - 100
            source = kmstest.Rect(0, 0, width, height)
            destination = kmstest.Rect(offset[0], offset[1], width, height)

            ret = self.atomic_plane_set(planes[1], crtc, source, destination, fb, sync=True)
            if ret < 0:
                self.fail(f'atomic plane set with offset {offset}')
                return

            self.logger.log(f'Moved overlay plane to {offset}')
            time.sleep(3)

        # Try to move the plane completely off-screen. The device is expected
        # to accept this and not to show the plane on the screen.
        offsets = ((mode.hdisplay, 50), (50, mode.vdisplay),
                   (-mode.hdisplay, 50), (50, -mode.vdisplay))
        for offset in offsets:
            width = fb.width - 100
            height = fb.height - 100
            source = kmstest.Rect(0, 0, width, height)
            destination = kmstest.Rect(offset[0], offset[1], width, height)

            ret = self.atomic_plane_set(planes[1], crtc, source, destination, fb, sync=True)
            if ret < 0:
                self.fail(f'atomic plane set with offset {offset}')
                return

            self.logger.log(f'Moved overlay plane to {offset}')
            time.sleep(3)

        # Disable and re-enable the plane when it is off-screen. The device is
        # still expected to handle this gracefully.
        ret = self.atomic_plane_disable(planes[1])
        if ret < 0:
            self.fail('off-screen atomic plane disable failed')
            return

        width = fb.width - 100
        height = fb.height - 100
        source = kmstest.Rect(0, 0, width, height)
        destination = kmstest.Rect(mode.hdisplay, 50, width, height)

        ret = self.atomic_plane_set(planes[1], crtc, source, destination, fb, sync=True)
        if ret < 0:
            self.fail('off-scrren atomic plane enable failed')
            return

        self.atomic_crtc_disable(crtc)
        self.success()


if __name__ == '__main__':
    PlanePositionTest().execute()

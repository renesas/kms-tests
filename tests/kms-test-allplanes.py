#!/usr/bin/python3
# SPDX-License-Identifier: GPL-2.0-or-later
# SPDX-FileCopyrightText: 2017-2019 Renesas Electronics Corporation

import kmstest
import pykms

class AllPlanesTest(kmstest.KMSTest):
    """Test composition with all planes enabled on all CRTCs."""

    def handle_page_flip(self, frame, time):
        self.logger.log('Page flip complete')

    def main(self):
        # Create the connectors to CRTCs map
        connectors = {}
        for connector in self.output_connectors():
            # Skip disconnected connectors
            if not connector.connected():
                continue

            # Add the connector to the map
            for crtc in connector.get_possible_crtcs():
                if crtc not in connectors:
                    connectors[crtc] = connector

        for crtc in self.card.crtcs:
            self.start(f'composition on CRTC {crtc.id}')

            # Get the connector and default mode
            try:
                connector = connectors[crtc];
                mode = connector.get_default_mode()
            except KeyError:
                self.skip('no connector or mode available')
                continue

            # List planes available for the CRTC
            planes = []
            for plane in self.card.planes:
                if plane.supports_crtc(crtc) and plane != crtc.primary_plane:
                    planes.append(plane)

            if len(planes) == 0:
                self.skip('no plane available for CRTC')
                continue

            self.logger.log(f'Testing connector {connector.fullname}, CRTC {crtc.id}, '
                            f'mode {mode.name} with {len(planes)} planes '
                            f'(P: {crtc.primary_plane.id}, O: {[plane.id for plane in planes]})')

            # Create a frame buffer
            fb = pykms.DumbFramebuffer(self.card, mode.hdisplay, mode.vdisplay, 'XR24')
            pykms.draw_test_pattern(fb)

            # Set the mode with a primary plane
            ret = self.atomic_crtc_mode_set(crtc, connector, mode, fb)
            if ret < 0:
                self.fail(f'atomic mode set failed with {ret}')
                continue

            self.run(3)

            # Add all other planes one by one
            offset = 100
            for plane in planes:
                source = kmstest.Rect(0, 0, fb.width, fb.height)
                destination = kmstest.Rect(offset, offset, fb.width, fb.height)
                ret = self.atomic_plane_set(plane, crtc, source, destination, fb)
                if ret < 0:
                    self.fail(f'atomic plane set failed with {ret}')
                    break

                self.logger.log(f'Adding plane {plane.id}')
                self.run(1)

                if self.flips == 0:
                    self.fail('No page flip registered')
                    break

                offset += 50

            else:
                self.success()

            self.atomic_crtc_disable(crtc)


if __name__ == '__main__':
    AllPlanesTest().execute()

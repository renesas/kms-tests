#!/usr/bin/python3
# SPDX-License-Identifier: GPL-2.0-or-later
# SPDX-FileCopyrightText: 2018-2019 Renesas Electronics Corporation

import kmstest
import pykms

class LegacyModeSetTest(kmstest.KMSTest):
    """Test mode setting on all connectors in sequence with the default mode through the legacy mode set API."""

    def handle_page_flip(self, frame, time):
        self.logger.log('Page flip complete')

    def main(self):
        for connector in self.output_connectors():
            self.start(f'legacy mode set on connector {connector.fullname}')

            # Skip disconnected connectors
            if not connector.connected():
                self.skip('unconnected connector')
                continue

            # Find a CRTC suitable for the connector
            crtc = connector.get_current_crtc()
            if not crtc:
                crtcs = connector.get_possible_crtcs()
                if len(crtcs) == 0:
                    pass

                crtc = crtcs[0]

            # Get the default mode for the connector
            try:
                mode = connector.get_default_mode()
            except ValueError:
                self.skip('no mode available')
                continue

            self.logger.log(f'Testing connector {connector.fullname} '
                            f'on CRTC {crtc.id} with mode {mode.name}')

            # Create a frame buffer
            fb = pykms.DumbFramebuffer(self.card, mode.hdisplay, mode.vdisplay, 'XR24')
            pykms.draw_test_pattern(fb)

            # Perform a mode set
            ret = crtc.set_mode(connector, fb, mode)
            if ret < 0:
                self.fail(f'legacy mode set failed with {ret}')
                continue

            self.logger.log('Legacy mode set complete')
            self.run(5)

            ret = crtc.disable_mode()
            if ret < 0:
                self.fail(f'legacy mode set disable failed with {ret}')
                continue

            self.success()


if __name__ == '__main__':
    LegacyModeSetTest().execute()

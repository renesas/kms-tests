#!/usr/bin/python3
# SPDX-License-Identifier: GPL-2.0-or-later
# SPDX-FileCopyrightText: 2019 Renesas Electronics Corporation

import kmstest
import pykms
import time

class FormatsTest(kmstest.KMSTest):
    """Test all available plane formats."""

    def main(self):
        self.start('plane formats')

        # Find a CRTC with a connected connector and at least one plane
        for connector in self.output_connectors():
            if not connector.connected():
                self.logger.log(f'unconnected connector {connector.fullname}')
                continue

            try:
                mode = connector.get_default_mode()
            except ValueError:
                continue

            crtcs = connector.get_possible_crtcs()
            for crtc in crtcs:
                if crtc.primary_plane:
                    break
            else:
                crtc = None

            if crtc:
                break

        else:
            self.skip('no CRTC available with connector')
            return

        self.logger.log(f'Testing connector {connector.fullname}, '
                        f'CRTC {crtc.id}, mode {mode.name}')

        failed = 0

        num_formats = len(crtc.primary_plane.formats)
        for i in range(num_formats):
            format = crtc.primary_plane.formats[i]

            self.logger.log(f'Testing format {format}')
            self.progress(i+1, num_formats)

            # Create a frame buffer
            try:
                fb = pykms.DumbFramebuffer(self.card, mode.hdisplay, mode.vdisplay, format)
            except ValueError:
                self.logger.log('Failed to create frame buffer')
                failed += 1
                continue

            pykms.draw_test_pattern(fb)

            # Set the mode with a primary plane
            ret = self.atomic_crtc_mode_set(crtc, connector, mode, fb)
            if ret < 0:
                self.logger.log(f'atomic mode set failed with {ret}')
                failed += 1
                continue

            self.run(3)

        self.atomic_crtc_disable(crtc)

        if failed:
            self.fail(f'{failed}/{num_formats} formats failed')
        else:
            self.success()


if __name__ == '__main__':
    FormatsTest().execute()

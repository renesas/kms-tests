#!/usr/bin/python3
# SPDX-License-Identifier: GPL-2.0-or-later
# SPDX-FileCopyrightText: 2017-2019 Renesas Electronics Corporation

import kmstest
import pykms

class ModeSetTest(kmstest.KMSTest):
    """Test mode setting on all connectors in sequence with the default mode."""

    def handle_page_flip(self, frame, time):
        self.logger.log('Page flip complete')

    def main(self):
        for connector in self.output_connectors():
            self.start(f'atomic mode set on connector {connector.fullname}')

            # Skip disconnected connectors
            if not connector.connected():
                self.skip('unconnected connector')
                continue

            # Find a CRTC suitable for the connector
            crtc = connector.get_current_crtc()
            if not crtc:
                crtcs = connector.get_possible_crtcs()
                if len(crtcs) == 0:
                    pass

                crtc = crtcs[0]

            # Get the default mode for the connector
            try:
                mode = connector.get_default_mode()
            except ValueError:
                self.skip('no mode available')
                continue

            self.logger.log(f'Testing connector {connector.fullname} '
                            f'on CRTC {crtc.id} with mode {mode.name}')

            # Create a frame buffer
            fb = pykms.DumbFramebuffer(self.card, mode.hdisplay, mode.vdisplay, 'XR24')
            pykms.draw_test_pattern(fb)

            # Perform a mode set
            ret = self.atomic_crtc_mode_set(crtc, connector, mode, fb)
            if ret < 0:
                self.fail(f'atomic mode set failed with {ret}')
                continue

            self.logger.log('Atomic mode set complete')
            self.run(5)
            self.atomic_crtc_disable(crtc)

            if self.flips == 0:
                self.fail('Page flip not registered')
            else:
                self.success()


if __name__ == '__main__':
    ModeSetTest().execute()
